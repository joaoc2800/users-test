package config

import (
	"fmt"
	"strings"

	"github.com/spf13/viper"
)

type (
	Config struct {
		Database `mapstructure:"database"`
	}

	Database struct {
		Host     string `env-required:"true" mapstructure:"host" env:"DATABASE_HOST"`
		User     string `env-required:"true" mapstructure:"username" env:"DATABASE_USERNAME"`
		Password string `env-required:"true" mapstructure:"password" env:"DATABASE_PASSWORD"`
		Port     string `env-required:"true" mapstructure:"port" env:"DATABASE_PORT"`
		DBName   string `env-required:"true" mapstructure:"dbname" env:"DATABASE_DBNAME"`
		SSLMode  string `env-required:"true" mapstructure:"ssl_mode" env:"DATABASE_SSL_MODE"`
	}
)

func GetConfig() (*Config, error) {
	cfg := &Config{}

	viper.SetConfigName("config")   // name of config file (without extension)
	viper.SetConfigType("yaml")     // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath("./config") // optionally look for config in the working directory

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %w", err))
	}

	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)
	viper.AutomaticEnv()
	err = viper.Unmarshal(&cfg)

	if err != nil {
		return nil, fmt.Errorf("Config error %s", err)
	}

	return cfg, nil
}
