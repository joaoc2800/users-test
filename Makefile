.DEFAULT_GOAL := help

export GOOSE_DBSTRING=postgresql://myuser:mypassword@localhost:5432/mydatabase?sslmode=disable
export GOOSE_DRIVER=postgres

export $(shell sed 's/=.*//' .env)
-include .env

.PHONY: help
help: ## Help command
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n"} /^[$$()% a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-25s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

.PHONY: run
run: ## Run application
	go run cmd/users/main.go

.PHONY: build
build: ## Build project
	go build -o bin/app cmd/users/main.go

.PHONY: format
format: ## Apply Golang format
	go fmt ./...

.PHONY: format-check
format-check: ## Check formatting with linter
	golangci-lint run

.PHONY: migrate
migrate: ## Run database migrations
	goose -dir ./infra/postgres/migrations up

.PHONY: migrate-rollback
migrate-rollback: ## Rollback database migrations
	goose -dir migrations down