package main

import (
	"fmt"
	"log"
	"net/http"
	"users/config"
	usersDB "users/infra/postgres"
	"users/internal/user"
	usersHttp "users/internal/user/transport/http"

	_ "github.com/lib/pq"
)

func main() {
	cfg, err := config.GetConfig()

	if err != nil {
		log.Fatal("Unable to load config:", err)
	}

	db, err := usersDB.Connect(cfg)
	if err != nil {
		log.Fatal("Error connecting to the database:", err)
	}

	defer func() {
		err := db.Close()

		if err != nil {
			log.Fatal("Error closing the database:", err)
		}
	}()

	repo := user.NewRepository(db)
	service := user.NewService(repo)

	usersHttp.MakeHandler(service)
	http.ListenAndServe(":8080", nil)

	//check graceful shutdown of APIs

	fmt.Println("User created successfully!")
}
