-- +goose Up
-- +goose StatementBegin
CREATE TYPE role AS ENUM ('admin', 'customer');

CREATE TABLE USERS
(
    id SERIAL PRIMARY KEY,
    first_name VARCHAR(36),
    last_name  VARCHAR(36),
    email TEXT,
    role ROLE,
    is_active BOOLEAN DEFAULT TRUE,
    created_at TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'utc'),
    updated_at TIMESTAMP WITHOUT TIME ZONE DEFAULT (NOW() AT TIME ZONE 'utc')
)
-- +goose StatementEnd

-- +goose Down
-- +goose StatementBegin
SELECT 'down SQL query';
-- +goose StatementEnd
