package user

import (
	"encoding/json"
	"errors"
	"time"
)

type Role string

const (
	Admin    Role = "admin"
	Customer Role = "customer"
)

type User struct {
	FirstName string
	LastName  string
	Email     string
	Role      Role
}

type UserDetail struct {
	User
	ID        int
	IsActive  bool
	CreatedAt time.Time
	UpdatedAt time.Time
}

type Reader interface {
	Get(id int) (*UserDetail, error)
	GetAll() ([]UserDetail, error)
}

type Writer interface {
	Create(User) (*UserDetail, error)
	Update(UserDetail) (*UserDetail, error)
	Delete(id int) error
}

type UserRepository interface {
	Reader
	Writer
}

type UseCase interface {
	Get(id int) (*UserDetail, error)
	GetAll() ([]UserDetail, error)
	Create(User) (*UserDetail, error)
	Update(User) (*UserDetail, error)
	Delete(id int) error
}

func (r *Role) UnmarshalJSON(data []byte) error {
	var roleStr string
	if err := json.Unmarshal(data, &roleStr); err != nil {
		return err
	}

	switch Role(roleStr) {
	case Admin, Customer:
		*r = Role(roleStr)
		return nil
	default:
		return errors.New("invalid role")
	}
}
