package user

type Service struct {
	repo Repository
}

func NewService(r Repository) *Service {
	return &Service{
		repo: r,
	}
}

func (s *Service) Create(user User) (*UserDetail, error) {
	userDetail, err := s.repo.Create(user)
	if err != nil {
		return nil, err
	}

	return userDetail, nil
}
