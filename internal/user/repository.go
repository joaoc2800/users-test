package user

import (
	"database/sql"
)

type Repository struct {
	db *sql.DB
}

func NewRepository(db *sql.DB) Repository {
	return Repository{db: db}
}

func (r Repository) Create(user User) (*UserDetail, error) {
	query := `INSERT INTO users (first_name, last_name, email, role)
              VALUES ($1, $2, $3, $4) 
              RETURNING id, first_name, last_name, email, role, is_active, created_at, updated_at`

	var userDetail UserDetail

	err := r.db.QueryRow(query, user.FirstName, user.LastName, user.Email, user.Role).
		Scan(&userDetail.ID, &userDetail.FirstName, &userDetail.LastName, &userDetail.Email, &userDetail.Role,
			&userDetail.IsActive, &userDetail.CreatedAt, &userDetail.UpdatedAt)

	if err != nil {
		return nil, err
	}

	return &userDetail, nil
}
