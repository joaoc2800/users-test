package http

import (
	"net/http"

	"users/internal/user"
)

func MakeHandler(svc *user.Service) {
	http.HandleFunc("/create-user", CreateEndpoint(*svc))
}
