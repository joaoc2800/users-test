package http

import (
	"encoding/json"
	"net/http"
	"users/internal/user"
)

type CreateRequest struct {
	FirstName string    `json:"firstName"`
	LastName  string    `json:"lastName"`
	Email     string    `json:"email"`
	Role      user.Role `json:"role"`
}

type CreateResponse struct {
	UserDetail *user.UserDetail `json:"userDetail"`
	Error      string           `json:"error,omitempty"`
}

func CreateEndpoint(svc user.Service) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		var req CreateRequest

		if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
			http.Error(w, "Invalid request", http.StatusBadRequest)
		}

		user := req.toUser()
		userDetail, err := svc.Create(user)
		if err != nil {
			w.Header().Set("Content-Type", "application/json")
			json.NewEncoder(w).Encode(CreateResponse{Error: err.Error()})
			return
		}

		w.Header().Set("Content-Type", "application/json")
		json.NewEncoder(w).Encode(CreateResponse{UserDetail: userDetail})
	}
}

func (req *CreateRequest) toUser() user.User {
	return user.User{
		FirstName: req.FirstName,
		LastName:  req.LastName,
		Email:     req.Email,
		Role:      req.Role,
	}
}
